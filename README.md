# plotly-can

Simulation pédagogique permettant d’étudier l’influence des paramètres de numérisation d’une tension sinusoïdale par des observations de graphiques.

Démonstration accessible à l'adresse:

https://benabel.frama.io/plotly-can/
