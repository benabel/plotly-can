import React from "react";
import logo from "./logo.svg";

import { FaGitlab } from "react-icons/fa";

export default () => (
  <div className="lyceum-footer text-center col">
    <a
      href="https://lyceum.fr"
      title="Créé pour lyceum.fr, un site open-source pour le lycée."
    >
      <img src={logo} alt="logo lyceum.fr" />
    </a>
    <a
      href="https://framagit.org/benabel/plotly-can"
      title="Code en licence libre MIT hébergé sur framagit grâce à l'association Framasoft: Libérons internet un octet à la fois"
    >
      <FaGitlab size="32px" />
    </a>
  </div>
);
