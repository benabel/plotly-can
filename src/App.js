import React from "react";
import Plot from "react-plotly.js";
import { Container, Row, Col } from "reactstrap";

import { generateData } from "./utils/data-generation";
import Analogic from "./components/Analogic";
import Numeric from "./components/Numeric";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const initialLayout = {
  title: "Conversion analogique numérique",
  titlefont: {
    size: 32,
    family:
      '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
  },
  autosize: true,
  showlegend: true,
  legend: {
    x: 0.4,
    y: -0.2,
  },
  yaxis: {
    title: "Tension analogique&nbsp;(V)",
    showgrid: false,
    // zeroline: false,
  },
  yaxis2: {
    title: "Tension numérique",
    titlefont: { color: "blue" },
    tickfont: { color: "blue" },
    // See https://github.com/d3/d3-format#locale_format
    tickformat: "",
    overlaying: "y",
    side: "right",
  },
};

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Umax: 6,
      durée: 0.01,
      nPoints: 100,
      f: 440,
      fe: 10000,
      res: 8,
      canUmax: 10,
      typeCodage: "",
      validate: "",
      data: [],
      layout: initialLayout,
      frames: [],
      config: { displayModeBar: true, responsive: true },
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validateBitRate = this.validateBitRate.bind(this);
  }

  componentDidMount() {
    this.setState({
      data: generateData(this.state),
    });
  }
  validateBitRate(event) {
    // débit binaire max fixé à 1Mo/s
    const maxBitRate = 8e6;
    // use event pour récupérer fe ou res
    const name = event.target.name;
    const value = event.target.value;
    const fe = name === "fe" ? value : this.state.fe;
    const res = name === "res" ? value : this.state.res;
    const bitRate = fe * res;
    let validate;
    if (bitRate > maxBitRate) {
      validate = "has-danger";
    } else {
      validate = "";
    }
    this.setState({ validate });
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    console.debug(`Changed: ${name} to  ${value}`);
    this.setState({
      [name]: value,
    });
    // Immutability ??
    // nécessaire pour appliquer les changements au graph
    // https://reactjs.org/docs/optimizing-performance.html#shouldcomponentupdate-in-action
    this.setState((state) => ({
      data: generateData(state),
      // Un peu lourdau car objet profond, on pourrait utiliser
      // https://github.com/kolodny/immutability-helper
      layout: Object.assign({}, state.layout, {
        yaxis2: Object.assign({}, state.layout.yaxis2, {
          tickformat: state.typeCodage,
        }),
      }),
    }));
  }

  render() {
    return (
      <Container fluid={true}>
        <Row className="app-container">
          <Col xs={{ size: 12, order: 2 }} sm={{ size: 6, order: 2 }} lg="3">
            <Analogic
              Umax={this.state.Umax}
              f={this.state.f}
              durée={this.state.durée}
              handleInputChange={this.handleInputChange}
            />
          </Col>
          <Col
            className="graph"
            xs={{ size: 12, order: 1 }}
            sm={{ size: 12, order: 1 }}
            lg={{ size: 6, order: 2 }}
          >
            <Plot
              style={{ width: "100%", height: "100%" }}
              useResizeHandler
              data={this.state.data}
              layout={this.state.layout}
              config={this.state.config}
            />
          </Col>
          <Col xs={{ size: 12, order: 3 }} sm={{ size: 6, order: 3 }} lg="3">
            <Numeric
              fe={this.state.fe}
              res={this.state.res}
              validate={this.state.validate}
              canUmax={this.state.canUmax}
              typeCodage={this.state.typeCodage}
              handleInputChange={this.handleInputChange}
              validateBitRate={this.validateBitRate}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}
