import React from "react";

import { Col, Form, FormGroup, Label, Input } from "reactstrap";
import Footer from "./footer-git";

export default class Analogic extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  handleInputChange(event) {
    this.props.handleInputChange(event);
  }
  render() {
    return (
      <div className="form-container">
        <h2>Paramètres du signal analogique</h2>
        <Form className="form">
          <Col>
            <FormGroup>
              <Label>Tension maximale (V)</Label>
              <Input
                name="Umax"
                type="number"
                value={this.props.Umax}
                onChange={this.handleInputChange}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Fréquence (Hz)</Label>
              <Input
                name="f"
                type="number"
                value={this.props.f}
                onChange={this.handleInputChange}
              />
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Durée&nbsp;(s)</Label>
              <Input
                name="durée"
                type="number"
                value={this.props.durée}
                onChange={this.handleInputChange}
              />
            </FormGroup>
          </Col>
        </Form>
        <Footer />
      </div>
    );
  }
}
