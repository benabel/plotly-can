function generateAnalogic(durée, f, Umax, nPoints) {
  let data = [];
  for (let i = 0; i < nPoints; i++) {
    let point = {};
    const time = (i * durée) / (nPoints - 1);
    point.time = time;
    point.analogic = Umax * Math.sin(2 * Math.PI * f * time);
    data.push(point);
  }
  return data;
}

/**
 * Quantification d'une grandeur analogique par la méthode d'arrondi
 *
 * https://en.wikipedia.org/wiki/Quantization_(signal_processing)#Rounding_example
 *
 * @param {*} x
 * @param {*} pas
 */
function quantify(x, pas) {
  return Math.floor(x / pas + 0.5);
}
/**
 * Crée le signal numérique
 *
 *
 *
 * @param {number} [durée] Durée d'acquisition en secondes
 * @param {number} [f] Fréquence du signal mesuré en Hz
 * @param {number} [fe] Fréquence d'échantillonage en Hz
 * @param {number} [res] Résolution du CAN en bits
 * @param {number} [Umax] Tension maximale mesurée en volt
 * @param {number} [canUmax] Tension max ie Intervalle de mesure: [-Umax, + Umax] en volt
 * @returns
 * Array of Objects
 */
function generateNumeric(durée, f, fe, res, Umax, canUmax, validate) {
  let data = [];
  const nPoints = fe * durée + 1;
  const pas = (2 * canUmax) / (2 ** res - 1);
  // On ne génére pas les données si bitrate dépassé
  if (validate === "has-danger") {
    return [];
  }
  for (let i = 0; i < nPoints; i++) {
    let point = {};
    const time = (i * durée) / (nPoints - 1);
    point.time = time;
    // On décale pour valeurs positives
    let analogic = Umax * Math.sin(2 * Math.PI * f * time); // + canUmax;

    let numeric;
    if (analogic < -canUmax) {
      analogic = -canUmax;
    } else if (analogic > canUmax) {
      analogic = canUmax;
    }
    numeric = quantify(analogic, pas);

    point.numeric = numeric + 2 ** (res - 1) - 1;
    data.push(point);
  }
  return data;
}

export function generateData(stateArg) {
  const { Umax, durée, nPoints, f, fe, res, canUmax, validate } = stateArg;
  const analogic = generateAnalogic(durée, f, Umax, nPoints);
  const numeric = generateNumeric(durée, f, fe, res, Umax, canUmax, validate);
  return [
    {
      x: analogic.map((x) => x.time),
      y: analogic.map((x) => x.analogic),
      type: "scatter",
      name: "analogique",
      mode: "lines",
      line: {
        shape: "spline",
        color: "red",
        width: 3,
        opacity: 0.7,
      },
    },
    {
      x: numeric.map((x) => x.time),
      y: numeric.map((x) => x.numeric),
      yaxis: "y2",
      type: "scatter",
      name: "numérique",
      mode: "lines+markers",
      line: { shape: "hv" },
      secondary_y: true,
      marker: {
        color: "blue",
        size: 4,
      },
      opacity: 0.7,
    },
  ];
}
