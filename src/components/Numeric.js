import React from "react";

import {
  Col,
  Form,
  FormGroup,
  FormFeedback,
  FormText,
  Label,
  Input,
} from "reactstrap";
import Footer from "./footer-cc-by-sa";

export default class Numeric extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validateBitRate = this.validateBitRate.bind(this);
  }
  handleInputChange(event) {
    this.props.handleInputChange(event);
  }
  validateBitRate(event) {
    this.props.validateBitRate(event);
  }
  render() {
    return (
      <div className="form-container">
        <h2>Paramètres du codage numérique</h2>
        <Form className="form">
          <Col>
            <FormGroup>
              <Label>Fréquence d'échantillonage&nbsp;(Hz)</Label>
              <Input
                name="fe"
                type="number"
                value={this.props.fe}
                invalid={this.props.validate === "has-danger"}
                onChange={(e) => {
                  this.validateBitRate(e);
                  this.handleInputChange(e);
                }}
              />
              <FormText>Nombre de mesures effectuées par seconde</FormText>
              <FormFeedback invalid>
                Attention le débit binaire ne peut dépasser 1 Mo/s
              </FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Résolution&nbsp;(bit)</Label>
              <Input
                type="select"
                name="res"
                value={this.props.res}
                invalid={this.props.validate === "has-danger"}
                onChange={(e) => {
                  this.validateBitRate(e);
                  this.handleInputChange(e);
                }}
              >
                <option value="2">2&nbsp;</option>
                <option value="4">4&nbsp;</option>
                <option value="8">8&nbsp;</option>
                <option value="16">16&nbsp;</option>
                <option value="32">32&nbsp;</option>
              </Input>
              <FormText>Nombre de bits utilisé pour chaque mesure</FormText>
              <FormFeedback invalid>
                Diminuez la fréquence d'échantillonage ou la résolution.
              </FormFeedback>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Calibre</Label>
              <Input
                type="select"
                name="canUmax"
                value={this.props.canUmax}
                onChange={this.handleInputChange}
              >
                <option value="2">-2V/2V</option>
                <option value="5">-5V/5V</option>
                <option value="10">-10V/10V</option>
                <option value="20">-20V/20V</option>
              </Input>
              <FormText>Intervalle de tensions qui peut-être numérisé</FormText>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label>Type de codage numérique</Label>
              <Input
                type="select"
                name="typeCodage"
                value={this.props.typeCodage}
                onChange={this.handleInputChange}
              >
                <option value="">décimal</option>
                <option value="b">binaire</option>
                <option value="x">hexadécimal</option>
              </Input>
            </FormGroup>
          </Col>
        </Form>
        <Footer />
      </div>
    );
  }
}
